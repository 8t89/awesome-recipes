# Awesome Recipes Android App

The app uses :

  * MVVM architecture with Data binding
  * Dagger2 for Dependency Injection
  * Navigation component
  * Flow for Repository to ViewModel communication
  
#### Next steps

  * More unit tests
  * UI tests
  * Move the dagger modules to different android modules