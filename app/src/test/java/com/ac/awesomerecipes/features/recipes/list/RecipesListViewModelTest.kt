package com.ac.awesomerecipes.features.recipes.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ac.awesomerecipes.data.Result
import com.ac.awesomerecipes.data.local.models.Recipe
import com.ac.awesomerecipes.features.recipes.repository.RecipesRepository
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class RecipesListViewModelTest {

    private val mockFlow: Flow<Result<List<Recipe>>>? = flow {
        emit(Result.loading())
        emit(Result.success(listOf(recipe)))
    }


    val recipe = Recipe(
        "https://test",
        "Test",
        "https://test",
        listOf("Step 1", "Step 2"),
        listOf(0, 10, 10),
        listOf(
            Recipe.Ingredient("Ingredient 1", "1", "Type"),
            Recipe.Ingredient("Ingredient 1", "2", "Type")
        )
    )

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Mock
    lateinit var recipesRepository: RecipesRepository

    lateinit var viewModelInstance: RecipesListViewModel

    private val recipesObserver: Observer<List<Recipe>> = mock()

    @Captor
    private val recipeNavigationCaptor: ArgumentCaptor<Recipe>? = null
    private val recipeNavigationObserver: Observer<Recipe> = mock()

    @Captor
    private val selectedDifficultyCaptor: ArgumentCaptor<Recipe.Difficulty>? = null
    private val selectedDifficultyObserver: Observer<Recipe.Difficulty> = mock()

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(mainThreadSurrogate)
        viewModelInstance = RecipesListViewModel(recipesRepository)
        viewModelInstance.recipes.observeForever(recipesObserver)
        viewModelInstance.recipeNavigationEvent.observeForever(recipeNavigationObserver)
    }

    @Test
    fun onRecipeClicked() {
        viewModelInstance.onRecipeClicked(recipe, 0)
        recipeNavigationCaptor.run {
            verify(recipeNavigationObserver, times(1)).onChanged(recipeNavigationCaptor?.capture())
            assertEquals(recipe, this?.value)
        }
    }
}