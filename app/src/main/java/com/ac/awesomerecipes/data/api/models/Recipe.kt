package com.ac.awesomerecipes.data.api.models

data class Recipe(
    val imageURL: String?,
    val ingredients: List<Ingredient>?,
    val name: String,
    val originalURL: String?,
    val steps: List<String>?,
    val timers: List<Int>?
) {
    data class Ingredient(
        val name: String?,
        val quantity: String?,
        val type: String?
    )
}