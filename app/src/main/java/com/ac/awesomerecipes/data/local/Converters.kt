package com.ac.awesomerecipes.data.local

import androidx.room.TypeConverter
import com.ac.awesomerecipes.data.local.models.Recipe
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {
    @TypeConverter
    fun intListFromString(string: String?) = string?.split(SEPARATOR)?.map { it.toInt() }

    @TypeConverter
    fun intListToString(list: List<Int>?) = list?.joinToString(SEPARATOR)

    @TypeConverter
    fun stringListFromString(string: String?) = string?.split(SEPARATOR)?.map { it }

    @TypeConverter
    fun stringListToString(list: List<String>?) = list?.joinToString(SEPARATOR)

    @TypeConverter
    fun ingredientsToString(list: List<Recipe.Ingredient>): String = Gson().toJson(list)

    @TypeConverter
    fun stringToIngredients(string: String): List<Recipe.Ingredient> =
        Gson().fromJson(string, object : TypeToken<List<Recipe.Ingredient>>() {}.type)

    companion object {
        private const val SEPARATOR = "###"
    }

}