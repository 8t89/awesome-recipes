package com.ac.awesomerecipes.data.local.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "recipes")
data class Recipe(
    val imageURL: String?,
    @PrimaryKey
    val name: String,
    val originalURL: String?,
    val steps: List<String>?,
    val timers: List<Int>?,
    val ingredients: List<Ingredient>
) : Parcelable {
    val difficulty: Difficulty
        get() = calculateDifficulty()

    val totalDuration: Int
        get() = timers?.sum() ?: 0


    private fun calculateDifficulty(): Difficulty {
        return when ((steps?.size ?: 0) + (timers?.sum() ?: 0) / 10 + (ingredients.size)) {
            in 0..10 -> Difficulty.Easy
            in 10..20 -> Difficulty.Medium
            else -> Difficulty.Hard
        }
    }

    @Parcelize
    data class Ingredient(
        val name: String?,
        val quantity: String?,
        val type: String?
    ) : Parcelable

    enum class Difficulty(val label: String) {
        Hard("Hard"),
        Medium("Medium"),
        Easy("Easy"),
        None("None");

        override fun toString(): String {
            return this.label
        }
    }

    enum class Duration(val label: String, val range: IntRange) {
        TenMax("0-10 Minutes", 1..9),
        TwentyMax("10-20 Minutes", 10..19),
        TwentyOver("20+", 20..Int.MAX_VALUE),
        None("None", 0..0);

        override fun toString(): String {
            return this.label
        }
    }

}