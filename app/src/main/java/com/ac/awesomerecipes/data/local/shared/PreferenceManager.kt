package com.ac.awesomerecipes.data.local.shared

import java.util.*

interface PreferenceManager {
    fun getCachedDataTime(): Date?
    fun saveCachedDataTime(dateTime: Date)
}