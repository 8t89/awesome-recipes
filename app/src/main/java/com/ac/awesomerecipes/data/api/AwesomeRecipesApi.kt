package com.ac.awesomerecipes.data.api

import com.ac.awesomerecipes.data.api.models.Recipe
import retrofit2.Response
import retrofit2.http.GET

interface AwesomeRecipesApi {
    @GET("/sampleapifortest/recipes.json")
    suspend fun getRecipes(): Response<List<Recipe>>
}