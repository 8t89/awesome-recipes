package com.ac.awesomerecipes.data.local.shared

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.ac.awesomerecipes.common.ext.toDate
import com.ac.awesomerecipes.common.ext.toString
import java.util.*

class AppPreferenceManager(context: Context) : PreferenceManager {

    var sharedPreferences: SharedPreferences

    init {
        sharedPreferences =
            context.getSharedPreferences(SHARE_PREFERENCES_NAME, Context.MODE_PRIVATE)
    }

    override fun getCachedDataTime(): Date? {
        val dateString = sharedPreferences.getString(RECIPE_CACHE_TIME_KEY, null)
        return dateString?.let {
            dateString.toDate(CACHE_TIME_FORMAT)
        }
    }

    override fun saveCachedDataTime(dateTime: Date) {
        sharedPreferences.edit {
            putString(RECIPE_CACHE_TIME_KEY, dateTime.toString(CACHE_TIME_FORMAT))
        }
    }


    companion object {
        const val SHARE_PREFERENCES_NAME = "recipes_shared"
        const val RECIPE_CACHE_TIME_KEY = "RECIPE_CACHE_TIME_KEY"
        const val CACHE_TIME_FORMAT = "yyyyMMddHHmmss"
    }
}