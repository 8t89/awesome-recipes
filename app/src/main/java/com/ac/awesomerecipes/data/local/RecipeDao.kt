package com.ac.awesomerecipes.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ac.awesomerecipes.data.local.models.Recipe


@Dao
abstract class RecipeDao {
    @Insert
    abstract fun insert(recipeEntity: Recipe?)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertAll(recipes: List<Recipe>)

    @Query("SELECT * FROM recipes")
    abstract fun getAllRecipes(): List<Recipe>

    @Query("DELETE FROM recipes")
    abstract fun deleteAll()
}