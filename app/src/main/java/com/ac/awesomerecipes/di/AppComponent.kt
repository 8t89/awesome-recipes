package com.ac.awesomerecipes.di

import android.app.Application
import com.ac.awesomerecipes.AwesomeRecipesApp
import com.ac.awesomerecipes.di.network.NetworkModule
import com.ac.awesomerecipes.di.storage.StorageModule
import com.ac.awesomerecipes.features.recipes.RecipesModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        AndroidSupportInjectionModule::class,
        NetworkModule::class,
        StorageModule::class,
        RecipesModule::class
    ]
)
interface AppComponent : AndroidInjector<AwesomeRecipesApp> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}