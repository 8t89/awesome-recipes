package com.ac.awesomerecipes.di.storage

import android.app.Application
import androidx.room.Room
import com.ac.awesomerecipes.data.local.AppDatabase
import com.ac.awesomerecipes.data.local.RecipeDao
import com.ac.awesomerecipes.data.local.shared.AppPreferenceManager
import com.ac.awesomerecipes.data.local.shared.PreferenceManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class StorageModule {

    @Singleton
    @Provides
    fun providesRoomDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder<AppDatabase>(application, AppDatabase::class.java, "recipe-db")
            .build()
    }

    @Singleton
    @Provides
    fun provideRecipesDao(appDatabase: AppDatabase): RecipeDao {
        return appDatabase.recipeDao()
    }

    @Singleton
    @Provides
    fun provideSharedPrefences(application: Application): PreferenceManager {
        return AppPreferenceManager(application.applicationContext)
    }
}