package com.ac.awesomerecipes.features.recipes.details

import androidx.lifecycle.MutableLiveData
import com.ac.awesomerecipes.data.local.models.Recipe
import com.ac.awesomerecipes.ui.base.BaseViewModel
import javax.inject.Inject

class RecipeDetailsViewModel @Inject constructor() : BaseViewModel() {

    fun init(recipe: Recipe) {
        this.recipe.postValue(recipe)
    }

    val recipe: MutableLiveData<Recipe> = MutableLiveData()

}