package com.ac.awesomerecipes.features.recipes.repository

import com.ac.awesomerecipes.data.Result
import com.ac.awesomerecipes.data.local.models.Recipe
import kotlinx.coroutines.flow.Flow

interface RecipesRepository {
    fun getRecipes(
        searchText: String? = null,
        selectedDuration: Recipe.Duration?,
        selectedDifficulty: Recipe.Difficulty?
    ): Flow<Result<List<Recipe>>>
}