package com.ac.awesomerecipes.features.recipes.details

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.ac.awesomerecipes.BR
import com.ac.awesomerecipes.R
import com.ac.awesomerecipes.data.local.models.Recipe
import com.ac.awesomerecipes.databinding.FragmentRecipeDetailsBinding
import com.ac.awesomerecipes.ui.base.BaseFragment
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.view_item_ingredient.view.*
import kotlinx.android.synthetic.main.view_item_step.view.*
import javax.inject.Inject

class RecipeDetailsFragment : BaseFragment<FragmentRecipeDetailsBinding, RecipeDetailsViewModel>() {

    private val args: RecipeDetailsFragmentArgs by navArgs()

    override val bindingVariable: Int = BR.viewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val layoutResId: Int = R.layout.fragment_recipe_details

    override val viewModel: RecipeDetailsViewModel by viewModels { viewModelFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpObservers()
    }


    private fun setUpObservers() {
        viewModel.recipe.observe(this.viewLifecycleOwner, Observer {
            setUpIngredientsCell(it.ingredients)
            setUpStepsCells(it.steps)
        })
    }

    private fun setUpIngredientsCell(ingredients: List<Recipe.Ingredient>) {
        viewDataBinding?.llIngredients?.let { llIngredients ->
            llIngredients.removeAllViews()
            ingredients.forEach {
                val rowView = LayoutInflater.from(context)
                    .inflate(R.layout.view_item_ingredient, llIngredients, false)
                rowView.txtIngredientName.text = it.name
                rowView.txtIngredientQuantity.text = it.quantity
                llIngredients.addView(rowView)
            }
        }
    }

    private fun setUpStepsCells(steps: List<String>?) {
        viewDataBinding?.llSteps?.let { llSteps ->
            llSteps.removeAllViews()
            steps?.forEach {
                val rowView = LayoutInflater.from(context)
                    .inflate(R.layout.view_item_step, llSteps, false)
                rowView.txtStep.text = it
                llSteps.addView(rowView)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
        viewModel.init(args.recipe)
    }
}