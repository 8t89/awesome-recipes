package com.ac.awesomerecipes.features.recipes.list

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ac.awesomerecipes.common.SingleLiveEvent
import com.ac.awesomerecipes.data.Result
import com.ac.awesomerecipes.data.local.models.Recipe
import com.ac.awesomerecipes.features.recipes.repository.RecipesRepository
import com.ac.awesomerecipes.ui.base.BaseViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class RecipesListViewModel @Inject constructor(private val repository: RecipesRepository) :
    BaseViewModel() {

    val recipeNavigationEvent = SingleLiveEvent<Recipe>()

    val searchText = MutableLiveData<String>()

    val itemDifficulties: List<Recipe.Difficulty> =
        listOf(
            Recipe.Difficulty.None,
            Recipe.Difficulty.Hard,
            Recipe.Difficulty.Medium,
            Recipe.Difficulty.Easy
        )
    var selectedDifficulty = MutableLiveData(Recipe.Difficulty.None)

    val itemDurations = listOf(
        Recipe.Duration.None,
        Recipe.Duration.TenMax,
        Recipe.Duration.TwentyMax,
        Recipe.Duration.TwentyOver
    )
    var selectedDuration = MutableLiveData(Recipe.Duration.None)

    private var recipesSource = repository.getRecipes(
        searchText.value,
        selectedDuration = selectedDuration.value,
        selectedDifficulty = selectedDifficulty.value
    )
    val recipes = MediatorLiveData<List<Recipe>>()

    val isLoading = MutableLiveData<Boolean>()
    val errorMessage = SingleLiveEvent<String>()

    init {
        viewModelScope.launch {
            recipesSource.collect {
                when (it.status) {
                    Result.Status.SUCCESS -> mapRecipesToList(it.data)
                    Result.Status.ERROR -> updateError(it.message)
                    Result.Status.LOADING -> updateLoading()
                }
            }
        }
    }

    private fun mapRecipesToList(recipes: List<Recipe>?) {
        isLoading.postValue(false)
        recipes?.let { recipes ->
            this.recipes.postValue(recipes)
        }
    }

    private fun updateLoading() {
        isLoading.postValue(true)
    }

    private fun updateError(message: String?) {
        errorMessage.postValue(message)
    }

    fun onRecipeClicked(
        recipe: Recipe,
        position: Int
    ) {
        recipeNavigationEvent.postValue(recipe)
    }

    fun onDifficultyChanged(any: Any) {
        if (any is Recipe.Difficulty) {
            selectedDifficulty.postValue(any)
            onFiltersChanged(difficulty = any)
        }
    }

    fun onDurationChanged(any: Any) {
        if (any is Recipe.Duration) {
            selectedDuration.postValue(any)
            onFiltersChanged(duration = any)
        }
    }

    private fun onFiltersChanged(
        text: String? = null,
        duration: Recipe.Duration? = null,
        difficulty: Recipe.Difficulty? = null
    ) {
        viewModelScope.launch {
            repository.getRecipes(
                searchText.value,
                duration ?: selectedDuration.value,
                difficulty ?: selectedDifficulty.value
            ).collect {
                when (it.status) {
                    Result.Status.SUCCESS -> mapRecipesToList(it.data)
                    Result.Status.ERROR -> updateError(it.message)
                    Result.Status.LOADING -> updateLoading()
                }
            }
        }
    }

    fun onSearchTextChanged(searchText: CharSequence) {
        this.searchText.postValue(searchText.toString())
        onFiltersChanged(text = searchText.toString())
    }
}