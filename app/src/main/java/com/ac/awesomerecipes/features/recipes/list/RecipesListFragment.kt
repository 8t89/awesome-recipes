package com.ac.awesomerecipes.features.recipes.list

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.ac.awesomerecipes.BR
import com.ac.awesomerecipes.R
import com.ac.awesomerecipes.databinding.FragmentRecipesListBinding
import com.ac.awesomerecipes.ui.base.BaseFragment
import com.ac.awesomerecipes.ui.widget.GenericAdapter
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class RecipesListFragment : BaseFragment<FragmentRecipesListBinding, RecipesListViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val layoutResId: Int = R.layout.fragment_recipes_list

    override val bindingVariable: Int = BR.viewModel

    override val viewModel: RecipesListViewModel by viewModels { viewModelFactory }

    private val recipesAdapter by lazy {
        GenericAdapter(R.layout.view_item_recipe, viewModel::onRecipeClicked)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.recipeNavigationEvent.observe(this.viewLifecycleOwner, Observer {
            findNavController().navigate(
                RecipesListFragmentDirections.actionRecipesListFragmentToRecipeDetailsFragment(
                    it
                )
            )
        })
        viewModel.errorMessage.observe(this.viewLifecycleOwner, Observer {
            Toast.makeText(this.context, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun setUpRecyclerView() {
        viewDataBinding?.rvRecipes?.let { rvRecipes ->
            rvRecipes.adapter = recipesAdapter
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

}