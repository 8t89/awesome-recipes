package com.ac.awesomerecipes.features.recipes

import androidx.lifecycle.ViewModel
import com.ac.awesomerecipes.di.ViewModelBuilder
import com.ac.awesomerecipes.di.ViewModelKey
import com.ac.awesomerecipes.features.recipes.details.RecipeDetailsFragment
import com.ac.awesomerecipes.features.recipes.details.RecipeDetailsViewModel
import com.ac.awesomerecipes.features.recipes.list.RecipesListFragment
import com.ac.awesomerecipes.features.recipes.list.RecipesListViewModel
import com.ac.awesomerecipes.features.recipes.repository.AppRecipesRepository
import com.ac.awesomerecipes.features.recipes.repository.RecipesRepository
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class RecipesModule {
    @ContributesAndroidInjector(
        modules = [ViewModelBuilder::class]
    )
    internal abstract fun recipesListFragment(): RecipesListFragment

    @ContributesAndroidInjector(
        modules = [ViewModelBuilder::class]
    )
    internal abstract fun recipeDetailsFragment(): RecipeDetailsFragment

    @Binds
    @IntoMap
    @ViewModelKey(RecipesListViewModel::class)
    abstract fun bindViewModel(viewModel: RecipesListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RecipeDetailsViewModel::class)
    abstract fun bindRecipeDetailsViewModel(viewModel: RecipeDetailsViewModel): ViewModel

    @Binds
    abstract fun bindRepository(repo: AppRecipesRepository): RecipesRepository
}