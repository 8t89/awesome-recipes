package com.ac.awesomerecipes.features.recipes.repository

import com.ac.awesomerecipes.common.ext.hour
import com.ac.awesomerecipes.common.ext.minus
import com.ac.awesomerecipes.data.Result
import com.ac.awesomerecipes.data.api.ApiDataSource.getResult
import com.ac.awesomerecipes.data.api.AwesomeRecipesApi
import com.ac.awesomerecipes.data.local.RecipeDao
import com.ac.awesomerecipes.data.local.models.Recipe
import com.ac.awesomerecipes.data.local.shared.PreferenceManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject
import com.ac.awesomerecipes.data.api.models.Recipe as ApiRecipe

class AppRecipesRepository @Inject constructor(
    private val recipesApi: AwesomeRecipesApi,
    private val recipeDao: RecipeDao,
    private val sharedPreferencesManager: PreferenceManager
) : RecipesRepository {

    override fun getRecipes(
        searchText: String?,
        selectedDuration: Recipe.Duration?,
        selectedDifficulty: Recipe.Difficulty?
    ): Flow<Result<List<Recipe>>> {
        return flow {
            emit(Result.loading())
            val cacheTime = sharedPreferencesManager.getCachedDataTime()
            if (cacheTime != null && cacheTime.after(Date().minus(1.hour))) {
                val recipes = withContext(Dispatchers.IO) {
                    recipeDao.getAllRecipes()
                }
                if (!recipes.isNullOrEmpty()) {
                    val filteredRecipes = getFilteredRecipes(
                        recipes,
                        searchText,
                        selectedDuration,
                        selectedDifficulty
                    )
                    emit(Result.success(filteredRecipes))
                } else {
                    fetchRecipesFromRemote(searchText, selectedDuration, selectedDifficulty)
                }
            } else {
                fetchRecipesFromRemote(searchText, selectedDuration, selectedDifficulty)
            }
        }
    }

    private fun getFilteredRecipes(
        recipes: List<Recipe>,
        searchText: String?,
        selectedDuration: Recipe.Duration?,
        selectedDifficulty: Recipe.Difficulty?
    ): List<Recipe> {
        println("**** $searchText AND $selectedDuration AND $selectedDifficulty")
        return recipes
            .filter { filterByString(searchText, it) }
            .filter { selectedDuration == null || selectedDuration == Recipe.Duration.None || it.totalDuration in selectedDuration.range }
            .filter { selectedDifficulty == null || selectedDifficulty == Recipe.Difficulty.None || it.difficulty == selectedDifficulty }
    }

    private fun filterByString(
        searchText: String?,
        recipe: Recipe
    ): Boolean {
        return searchText.isNullOrEmpty() || (recipe.name.contains(searchText)
                || recipe.ingredients.any { it.name?.contains(searchText) == true }
                || recipe.steps?.any { it.contains(searchText) } == true)

    }

    private suspend fun FlowCollector<Result<List<Recipe>>>.fetchRecipesFromRemote(
        searchText: String?,
        selectedDuration: Recipe.Duration?,
        selectedDifficulty: Recipe.Difficulty?
    ) {
        val responseStatus = getResult { recipesApi.getRecipes() }
        if (responseStatus.status == Result.Status.SUCCESS) {
            withContext(Dispatchers.IO) {
                recipeDao.deleteAll()
            }
            sharedPreferencesManager.saveCachedDataTime(Date())
            val savedRecipes = saveCallResultAndReturn(responseStatus.data!!)
            val filteredRecipes =
                getFilteredRecipes(savedRecipes, searchText, selectedDuration, selectedDifficulty)
            emit(Result.success(filteredRecipes))
        } else if (responseStatus.status == Result.Status.ERROR) {
            emit(Result.error(responseStatus.message!!))
        }
    }

    private suspend fun saveCallResultAndReturn(data: List<ApiRecipe>): List<Recipe> {
        val dbRecipeList = data.map(this::createDbRecipeFromApiRecipe)
        withContext(Dispatchers.IO) {
            recipeDao.insertAll(dbRecipeList)
        }
        return dbRecipeList
    }

    private fun createDbRecipeFromApiRecipe(apiRecipe: ApiRecipe): Recipe {
        return Recipe(
            apiRecipe.imageURL,
            apiRecipe.name,
            apiRecipe.originalURL,
            apiRecipe.steps,
            apiRecipe.timers,
            apiRecipe.ingredients?.map {
                Recipe.Ingredient(it.name, it.quantity, it.type)
            } ?: emptyList()
        )
    }

}