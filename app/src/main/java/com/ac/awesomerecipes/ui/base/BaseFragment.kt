package com.ac.awesomerecipes.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class BaseFragment<VB : ViewDataBinding, out VM : BaseViewModel> : Fragment() {

    abstract val viewModel: VM
    abstract val bindingVariable: Int

    protected var viewDataBinding: VB? = null

    @get:LayoutRes
    protected abstract val layoutResId: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<VB>(inflater, layoutResId, container, false).also {
            viewDataBinding = it
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewDataBinding?.setVariable(bindingVariable, viewModel)
        viewDataBinding?.lifecycleOwner = this@BaseFragment.viewLifecycleOwner
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        viewDataBinding = null
        super.onDestroyView()
    }
}