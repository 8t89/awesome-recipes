package com.ac.awesomerecipes.ui.bindingadapters

import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.ac.awesomerecipes.ui.widget.BindableAdapter

@Suppress("UNCHECKED_CAST")
object RecyclerViewBindingAdapters {
    @JvmStatic
    @BindingAdapter("data")
    fun <T> setRecyclerViewProperties(recyclerView: RecyclerView, items: LiveData<List<T>>?) {
        val adapter = recyclerView.adapter
        if (adapter is BindableAdapter<*>) {
            (adapter as BindableAdapter<T>).setData(items?.value.orEmpty())
        }
    }

    @JvmStatic
    @BindingAdapter("data")
    fun <T> setRecyclerViewProperties(recyclerView: RecyclerView, items: List<T>?) {
        val adapter = recyclerView.adapter
        if (adapter is BindableAdapter<*>) {
            (adapter as BindableAdapter<T>).setData(items.orEmpty())
        }
    }
}
