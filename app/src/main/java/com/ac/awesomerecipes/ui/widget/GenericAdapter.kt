package com.ac.awesomerecipes.ui.widget

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView

open class GenericAdapter<M>(@LayoutRes val layoutId: Int, private val listener: ((model: M, position: Int) -> Unit)? = null) :
    RecyclerView.Adapter<GenericAdapter.GenericViewHolder<M>>(), BindableAdapter<M> {

    private var items: List<M> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun setData(data: List<M>) {
        this.items = data
    }

    override fun onBindViewHolder(holder: GenericViewHolder<M>, position: Int) {
        holder.bind(items[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder<M> {
        val inflater = LayoutInflater.from(parent.context)
        return GenericViewHolder(
            DataBindingUtil.inflate<ViewDataBinding>(
                inflater,
                layoutId,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = items.count()

    class GenericViewHolder<M>(private val viewDataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root) {
        fun bind(viewModel: M, listener: ((model: M, position: Int) -> Unit)?) {
            viewDataBinding.setVariable(BR.viewModel, viewModel)
            viewDataBinding.executePendingBindings()
            if (listener != null) {
                viewDataBinding.root.setOnClickListener {
                    listener.invoke(
                        viewModel,
                        adapterPosition
                    )
                }
            }
        }
    }
}

interface BindableAdapter<M> {
    fun setData(data: List<M>)
}