package com.ac.awesomerecipes.ui.bindingadapters

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object ImageViewBindingAdapters {
    @JvmStatic
    @BindingAdapter(
        value = ["glideUrl", "placeholderDrawable", "errorDrawable"],
        requireAll = false
    )
    fun loadImage(
        view: ImageView,
        imageUrl: String?,
        placeholderDrawable: Drawable?,
        errorDrawable: Drawable?
    ) {
        if (imageUrl != null) {
            Glide.with(view)
                .load(imageUrl).apply {
                    if (placeholderDrawable != null) placeholder(placeholderDrawable)
                    if (errorDrawable != null) error(errorDrawable)
                }
                .into(view)
        } else {
            Glide.with(view).clear(view)
        }
    }
}