package com.ac.awesomerecipes.ui.bindingadapters

import android.widget.Spinner
import androidx.databinding.BindingAdapter
import com.ac.awesomerecipes.ui.ext.SpinnerExtensions
import com.ac.awesomerecipes.ui.ext.SpinnerExtensions.setSpinnerEntries
import com.ac.awesomerecipes.ui.ext.SpinnerExtensions.setSpinnerItemSelectedListener
import com.ac.awesomerecipes.ui.ext.SpinnerExtensions.setSpinnerValue

@BindingAdapter("entries")
fun Spinner.setEntries(entries: List<Any>?) {
    setSpinnerEntries(entries)
}

@BindingAdapter("onItemSelected")
fun Spinner.setItemSelectedListener(itemSelectedListener: SpinnerExtensions.ItemSelectedListener?) {
    setSpinnerItemSelectedListener(itemSelectedListener)
}

@BindingAdapter("newValue")
fun Spinner.setNewValue(newValue: Any?) {
    setSpinnerValue(newValue)
}