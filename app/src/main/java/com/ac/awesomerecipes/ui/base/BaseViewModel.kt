package com.ac.awesomerecipes.ui.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()