package com.ac.awesomerecipes.ui.bindingadapters

import android.widget.Spinner
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.ac.awesomerecipes.ui.ext.SpinnerExtensions.getSpinnerValue
import com.ac.awesomerecipes.ui.ext.SpinnerExtensions.setSpinnerInverseBindingListener
import com.ac.awesomerecipes.ui.ext.SpinnerExtensions.setSpinnerValue

@BindingAdapter("selectedValue")
fun Spinner.setSelectedValue(selectedValue: Any?) {
    setSpinnerValue(selectedValue)
}

@BindingAdapter("selectedValueAttrChanged")
fun Spinner.setInverseBindingListener(inverseBindingListener: InverseBindingListener?) {
    setSpinnerInverseBindingListener(inverseBindingListener)
}

object InverseSpinnerBindings {

    @JvmStatic
    @InverseBindingAdapter(attribute = "selectedValue", event = "selectedValueAttrChanged")
    fun Spinner.getSelectedValue(): Any? {
        return getSpinnerValue()
    }
}