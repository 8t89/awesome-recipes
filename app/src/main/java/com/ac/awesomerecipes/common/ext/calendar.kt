package com.ac.awesomerecipes.common.ext

import java.text.SimpleDateFormat
import java.util.*

internal val calendar: Calendar by lazy {
    Calendar.getInstance()
}

fun Date.toString(pattern: String): String? {
    val sdf = SimpleDateFormat(pattern, Locale.getDefault())
    return try {
        sdf.format(this)
    } catch (ex: Exception) {
        null
    }
}

fun String.toDate(pattern: String): Date? {
    val sdf = SimpleDateFormat(pattern, Locale.getDefault())
    return try {
        sdf.parse(this)
    } catch (ex: Exception) {
        null
    }
}

fun String.toDate(pattern: String, timeZone: TimeZone): Date? {
    val sdf = SimpleDateFormat(pattern, Locale.getDefault())
    sdf.timeZone = timeZone
    return try {
        sdf.parse(this)
    } catch (ex: Exception) {
        null
    }
}

val Date.timeWithZone: Long
    get() = time + TimeZone.getDefault().rawOffset + TimeZone.getDefault().dstSavings

operator fun Date.plus(duration: Duration): Date {
    calendar.time = this
    calendar.add(duration.unit, duration.value)
    return calendar.time
}

operator fun Date.minus(duration: Duration): Date {
    calendar.time = this
    calendar.add(duration.unit, -duration.value)
    return calendar.time
}

class Duration(internal val unit: Int, internal val value: Int) {
    val ago = calculate(from = Date(), value = -value)

    val since = calculate(from = Date(), value = value)

    private fun calculate(from: Date, value: Int): Date {
        calendar.time = from
        calendar.add(unit, value)
        return calendar.time
    }

    override fun hashCode() = Objects.hashCode(unit) * Objects.hashCode(value)

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is Duration) {
            return false
        }
        return unit == other.unit && value == other.value
    }
}


val Int.year: Duration
    get() = Duration(unit = Calendar.YEAR, value = this)

val Int.years: Duration
    get() = year

val Int.month: Duration
    get() = Duration(unit = Calendar.MONTH, value = this - 1)

val Int.months: Duration
    get() = month

val Int.week: Duration
    get() = Duration(unit = Calendar.WEEK_OF_MONTH, value = this)

val Int.weeks: Duration
    get() = week

val Int.day: Duration
    get() = Duration(unit = Calendar.DATE, value = this)

val Int.days: Duration
    get() = day

val Int.hour: Duration
    get() = Duration(unit = Calendar.HOUR_OF_DAY, value = this)

val Int.hours: Duration
    get() = hour

val Int.minute: Duration
    get() = Duration(unit = Calendar.MINUTE, value = this)

val Int.minutes: Duration
    get() = minute

val Int.second: Duration
    get() = Duration(unit = Calendar.SECOND, value = this)

val Int.seconds: Duration
    get() = second